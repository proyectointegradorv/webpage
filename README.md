# Web Page

### Página web para el proyecto integrador

#### Contiene:

- Inicio
    - Información sobre el proyecto integrador.
- Base de datos
    - Información de la base de datos, enlace al repositorio, diagrama de clases y modelo entidad-relación.
- Entregas
    - Información de las entregas, fechas.
- Estudiante
    - Información del estudiante(*).
- Práctica
    - Información de la práctica(*).
- Empresa
    - Información de la empresa(*).
- Usuarios
    - Información de los usuarios(*).
- Registro
    - Registro de cambios(*).
- Aplicación de escritorio
    - Información, enlace al código, modelo entidad relación e imágenes  de la aplicación de escritorio.
- Acerca de
    - Información de los integrantes.

(*) Work in Progress
