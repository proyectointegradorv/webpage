<?php

//Start the connection
function connection(){
//Variables for the connection
    $server = "localhost";
    $uname = "root";
    $password = "";
    $dbname = "practicaDB";

    $connect = new mysqli($server, $uname, $password, $dbname);

    //If the connection fails, finish the program
    if($connect->connect_error){
        die("Error de conexión\n");
    }

    return $connect;
}

/*****************************************************************************/
/*                                    Log                                    */
/*****************************************************************************/

//Query - Read Student
//function selectEst(){
    $connect = connection();

    $result = $connect->query("SELECT * FROM Logs");
    $rowcount=mysqli_num_rows($result);

    //output
    $output = "$rowcount";

    //Fetch array
    while($ver = $result->fetch_array(MYSQLI_ASSOC)){
        if($output != ""){
            $output .= ",";
        }

        $output .= '["'.$ver["cod"].'",';
        $output .= '"'.$ver["uname"].'",';
        $output .= '"'.$ver["dateLog"].'",';
        $output .= '"'.$ver["timeLog"].'",';
        $output .= '"'.$ver["typeLog"].'",';
        $output .= '"'.$ver["query"].'"]';

        $myArr = array();
    }

    //echo nl2br("$output");
    $output ='[['.$output.']]';

    echo"$output";

    $file = 'Logs.json';
    file_put_contents($file, $output);

//}

//selectEst();
//insertEst();
//selectEst();
//updateEst();
//selectEst();
//deleteEst();
//selectEst();

?>
