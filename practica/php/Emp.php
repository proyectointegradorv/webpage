<?php

//Start the connection
function connection(){
//Variables for the connection
    $server = "localhost";
    $uname = "root";
    $password = "";
    $dbname = "practicaDB";

    $connect = new mysqli($server, $uname, $password, $dbname);

    //If the connection fails, finish the program
    if($connect->connect_error){
        die("Error de conexión\n");
    }

    return $connect;
}

/*****************************************************************************/
/*                                 Empresa                                   */
/*****************************************************************************/

//Query - Read Company
$connect = connection();

$result = $connect->query("SELECT * FROM tbl_empresa ORDER BY(ID) asc");

$rowcount=mysqli_num_rows($result);

//output
$output = "$rowcount";

//Fetch array
while($ver = $result->fetch_array(MYSQLI_ASSOC)){
    if($output != ""){
        $output .= ",";
    }

    $output .= '["'.$ver["ID"].'",';
    $output .= '"'.$ver["NIT"].'",';
    $output .= '"'.$ver["nombre_empresa"].'",';
    $output .= '"'.$ver["ciudad_sede"].'",';
    $output .= '"'.$ver["departamento_sede"].'",';
    $output .= '"'.$ver["pais_sede"].'",';
    $output .= '"'.$ver["tipo_empresa"].'",';
    $output .= '"'.$ver["direccion_sede"].'",';
    $output .= '"'.$ver["tipo_contacto"].'",';
    $output .= '"'.$ver["numero_contacto"].'",';
    $output .= '"'.$ver["website_contacto"].'",';
    $output .= '"'.$ver["estado_empresa"].'",';
    $output .= '"'.$ver["origen_empresa"].'",';
    $output .= '"'.$ver["numero_empleados"].'"]';

    $myArr = array();
}

//echo nl2br("$output");
$output ='[['.$output.']]';

echo"$output";

$file = 'Emp.json';
file_put_contents($file, $output);
?>

