<?php

//Start the connection
function connection(){
//Variables for the connection
    $server = "localhost";
    $uname = "root";
    $password = "";
    $dbname = "practicaDB";

    $connect = new mysqli($server, $uname, $password, $dbname);

    //If the connection fails, finish the program
    if($connect->connect_error){
        die("Error de conexión\n");
    }

    return $connect;
}

/*****************************************************************************/
/*                              Estudiante                                   */
/*****************************************************************************/

/*
//Query - Insert
function insertEst(){
    $connect = connection();

    $insert = "INSERT INTO Est(cod, doc, tipoDoc, priNom, segNom, terNom, priApe, secApe," .
        " sexo, fecNac, lugNac, carrera, semestre, dirResi, barrioResi," .
                         " tel, email)" .
              " VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $pre = mysqli_prepare($connect, $insert);

    //echo "$insert";
    mysqli_stmt_bind_param($pre, "ssssssssssssssss", $doc, $tipoDoc, $priNom, $segNom,
                     $terNom, $priApe, $secApe, $sexo, $fecNac, $lugNac, $carrera,
                     $semestre, $dirResi, $barrioResi, $tel, $email);
    $doc = "e";
    $tipoDoc = "e";
    $priNom = "e";
    $segNom = "e";
    $terNom = "e";
    $priApe = "e";
    $secApe = "e";
    $sexo = "e";
    $fecNac = "e";
    $lugNac = "e";
    $carrera = "e";
    $semestre = "e";
    $dirResi = "e";
    $barrioResi = "e";
    $tel = "e";
    $email = "e";


    mysqli_stmt_execute($pre);

    printf("%d row inserted.\n", $pre->affected_rows);

    mysqli_stmt_close($pre);

    mysqli_close($connect);
}



//Query - Update
function updateEst(){
    $connect = connection();

    $update = "UPDATE Est SET
                tipoDoc = ?, 
                priNom = ?, 
                segNom = ?, 
                terNom = ?, 
                priApe = ?, 
                secApe = ?, 
                sexo = ?, 
                fecNac = ?, 
                lugNac = ?, 
                carrera = ?, 
                semestre = ?, 
                dirResi = ?, 
                barrioResi = ?, 
                tel = ?, 
                email = ? 
                WHERE doc = ?";

    $pre = mysqli_prepare($connect, $update);

    //echo "$update";
    mysqli_stmt_bind_param($pre, "ssssssssssssssss", $tipoDoc, $priNom, $segNom,
                     $terNom, $priApe, $secApe, $sexo, $fecNac, $lugNac, $carrera,
                     $semestre, $dirResi, $barrioResi, $tel, $email, $doc);
    $tipoDoc = "r";
    $priNom = "r";
    $segNom = "r";
    $terNom = "r";
    $priApe = "r";
    $secApe = "r";
    $sexo = "r";
    $fecNac = "r";
    $lugNac = "r";
    $carrera = "r";
    $semestre = "r";
    $dirResi = "r";
    $barrioResi = "r";
    $tel = "r";
    $email = "r";
    $doc = "e";


    mysqli_stmt_execute($pre);

    printf("%d row updated.\n", $pre->affected_rows);

    mysqli_stmt_close($pre);

    mysqli_close($connect);
}



//Query - Delete
function deleteEst(){

    $connect = connection();

    $delete = "DELETE FROM Est WHERE doc = ?";

    $pre = mysqli_prepare($connect, $delete);

    //echo "$delete";
    mysqli_stmt_bind_param($pre, "s", $doc);
    $doc = "e";

    mysqli_stmt_execute($pre);

    printf("%d row deleted.\n", $pre->affected_rows);

    mysqli_stmt_close($pre);

    mysqli_close($connect);
}
*/

//Query - Read Student
//function selectEst(){
    $connect = connection();

    $result = $connect->query("SELECT * FROM Est");

    $rowcount=mysqli_num_rows($result);

    //output
    $output = "$rowcount";

    //Fetch array
    while($ver = $result->fetch_array(MYSQLI_ASSOC)){
        if($output != ""){
            $output .= ",";
        }

        $output .= '["'.$ver["doc"].'",';
        $output .= '"'.$ver["tipoDoc"].'",';
        $output .= '"'.$ver["priNom"].'",';
        $output .= '"'.$ver["segNom"].'",';
        $output .= '"'.$ver["terNom"].'",';
        $output .= '"'.$ver["priApe"].'",';
        $output .= '"'.$ver["secApe"].'",';
        $output .= '"'.$ver["sexo"].'",';
        $output .= '"'.$ver["fecNac"].'",';
        $output .= '"'.$ver["lugNac"].'",';
        $output .= '"'.$ver["carrera"].'",';
        $output .= '"'.$ver["semestre"].'",';
        $output .= '"'.$ver["dirResi"].'",';
        $output .= '"'.$ver["barrioResi"].'",';
        $output .= '"'.$ver["tel"].'",';
        $output .= '"'.$ver["email"].'"]';

        $myArr = array();
    }

    //echo nl2br("$output");
    $output ='[['.$output.']]';

    echo"$output";

    $file = 'Est.json';
    file_put_contents($file, $output);

//}

//selectEst();
//insertEst();
//selectEst();
//updateEst();
//selectEst();
//deleteEst();
//selectEst();

?>
